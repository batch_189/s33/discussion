
// // fetch is to retrieve data from an API - fetch return as a promise. .then() is the method, the function is to breakdown what to be fetched.
    let posts = fetch('https://jsonplaceholder.typicode.com/posts')
    // convert response/data into a .json()
        .then((response) => response.json())
    // data is what we get on the response.json().
        .then((data) => console.log(data))

    console.log(posts);

// // Async and await are the JS ES6 equivalent of .then() syntax, allow us to write clean code while utilizing as API fetching functionality. It basically makes our code asynchronous.
    fetchPost = async() => {
        let result = await fetch ('https://jsonplaceholder.typicode.com/posts');
        console.log(result);

        let json = await result.json();
        console.log(json);
    };

    fetchPost();

// Get a single post from the API
    fetch ('https://jsonplaceholder.typicode.com/posts/10')
        .then((response) => response.json())
        .then((data) => console.log(data))

// Creating a single post
/*
    we can specify the method using a second object arg within fetch function. 
    Besides the method, we can also specify the header and the body of the request.
// */
    fetch ('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST', 
        headers: {
            'content-Type': 'application/json'
        },
        body: JSON.stringify({
            userId: 1,
            title: "New Post",
            body: "Hello World"
        })
    })
        .then((response) => response.json())
        .then((data) => console.log(data))

// Updating existing data from server/database using the PUT method
/*
    When updating existing data, you require 2 things:
     1. ID of the post to be updated
     2. New body/content of the post to be updated.
*/
    fetch ('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        headers: {
            'content-Type': 'application/json'
        },
        body: JSON.stringify({
                title: 'Updated post'
        }) 
    })
        .then((response) => response.json())
        .then((data) => console.log(data))

// Deleting an existing data from server/database using delete method
    fetch ('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE'
    })
        .then((response) => response.json())
        .then((data) => console.log(data))

// Filtering/query existing data using '?' syntax after the endpoint
    fetch ('https://jsonplaceholder.typicode.com/posts?userId=1')
        .then((response) => response.json())
        .then((data) => console.log(data))

// fetch comment from a specific post by using Id of the post as well as using the '/comments'
    fetch ('https://jsonplaceholder.typicode.com/posts/1/comments')
        .then((response) => response.json())
        .then((data) => console.log(data))

